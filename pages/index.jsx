import Head from "next/head";
import axios from "axios";
import ProductList from "../components/product-list/product-list";

export async function getServerSideProps() {
  const response = await axios.get("https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI");
  const data = response.data;
  return {
    props: {
      data: data,
    },
  };
}

const HomePage = ({ data }) => {
  if (!data) return <h1>Loading...</h1>;
  if(!data.products) return <h1>No items!</h1>;

  // Only show first 20 products as per test instruction
  let items = data.products.slice(0, 20);

  return (
    <>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <h1>Dishwashers</h1>
      <ProductList items={items} />
    </>
  );
};

export default HomePage;
