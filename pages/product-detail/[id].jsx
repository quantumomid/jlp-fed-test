import axios from "axios";
import ProductDetailArticle from "../../components/product-detail/product-detail-article";
import Head from "next/head";

export async function getServerSideProps(context) {
  const id = context.params.id;
  const response = await axios.get("https://api.johnlewis.com/mobile-apps/api/v1/products/" + id);
  const data = response.data;

  return {
    props: { productData: data },
  };
}



const ProductDetail = ({ productData }) => {
  // console.log({productData});
  if (!productData) return <h1>Loading...</h1>;
  if(!productData.title || productData.title.length===0) return <h1>Item not found!</h1>;

  return (
    <>
      <Head>
        <title>{productData.title}</title>
      </Head>
      <ProductDetailArticle productData={productData} />
    </>
  );
};

export default ProductDetail;
