import React from "react";
import { render, screen } from "@testing-library/react";
import mockDataProduct from "../../mockData/mockDataProduct.json";
import ProductDetailArticle from "./product-detail-article";

describe("First Snapshot Test Case", () => {
    test("Testing the Product Detail Article UI", () => {
        const { asFragment } = render(<ProductDetailArticle productData={mockDataProduct} />);
        // screen.debug;
        expect(asFragment()).toMatchSnapshot();
    })
});