import styles from "./product-detail-info.module.scss";

const ProductDetailInfo = ({ productInformationString, productCode }) => (
    <section className={styles.productDetails}>
        <h3>Product information</h3>
        <div className={styles.productDetailsInfo}>
            <p>{productInformationString}</p>
            <p className={styles.productCode}>Product code: {productCode}</p>
        </div>
    </section>
)

export default ProductDetailInfo;