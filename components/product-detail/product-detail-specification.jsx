import styles from "./product-detail-specification.module.scss";

const ProductDetailSpecification = ({ attributes }) => (
    <section className={styles.productSpecification}>
        <h3>Product specification</h3>
        <ul>
            {attributes.map((item) => (
                <li key={item.name + item.value}>
                    <span className={styles.productFeature}>{item.name}</span> <span className={styles.productValue}>{item.value}</span>
                </li>
            ))}
        </ul>
    </section>
);

export default ProductDetailSpecification;