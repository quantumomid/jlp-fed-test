import BackButton from "../back-button/back-button";
import ProductCarousel from "../product-carousel/product-carousel";
import styles from "./product-detail-article.module.scss";
import ProductDetailInfo from "./product-detail-info";
import ProductDetailSpecification from "./product-detail-specification";

const ProductDetailArticle = ({productData}) => {
    // Removing html tags from product information string 
    const regex = /(<([^>]+)>)/ig
    const productInformationString = productData.details.productInformation.replace(regex, "");

    return (
        <article className={styles.article}>
            <header className={styles.header}>
                <BackButton />
                <h1 className={styles.title}>{productData.title}</h1>
            </header>
        
            <div className={styles.landscapeContainer}>
                <div className={styles.rightSide}>
                    {/* For landscape */}
                    <section className={`${styles.offerInfo} ${styles.landscape}`}>
                            <h2 className={styles.price}>£{productData.price.now}</h2>
                            <p className={styles.specialOffer}>{productData.displaySpecialOffer}</p>
                            <p className={styles.includedServices}>{productData.additionalServices.includedServices}</p>                
                    </section>
                </div>

                <div className={styles.leftSide}>
                    <ProductCarousel images={productData.media.images.urls} altText={productData.title} />

                    <hr className={`${styles.divider} ${styles.portrait}`} />

                    <div className={styles.productInfoContainer}>
                        {/* For portrait */}
                        <section className={`${styles.offerInfo} ${styles.portrait}`}>
                            <h2 className={styles.price}>£{productData.price.now}</h2>
                            <p className={styles.specialOffer}>{productData.displaySpecialOffer}</p>
                            <p className={styles.includedServices}>{productData.additionalServices.includedServices}</p>
                        </section>

                        <ProductDetailInfo productInformationString={productInformationString} productCode={productData.code} />

                        <section className={styles.readMore}>
                            <h3>Read more</h3>
                            <BackButton />
                        </section>

                        <ProductDetailSpecification attributes={productData.details.features[0].attributes} />

                    </div>
                </div>
            </div>

        </article>
    );
}

export default ProductDetailArticle;