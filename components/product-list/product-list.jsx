import Link from "next/link";
import ProductListItem from "../product-list-item/product-list-item";
import styles from "./product-list.module.scss";

const ProductList = ({ items }) => {
    return (
        <div className={styles.content} data-testid="product-list-items-container">
            {items.map((item) => (
            <Link
                key={item.productId}
                href={{
                pathname: "/product-detail/[id]",
                query: { id: item.productId },
                }}
            >
                <a className={styles.link}>
                <ProductListItem 
                    image={item.image}
                    price={item.variantPriceRange.display.max}
                    description={item.title}
                />
                </a>
            </Link>
            ))}
        </div>
    )
}

export default ProductList;