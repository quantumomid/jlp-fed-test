import { Carousel } from 'react-responsive-carousel';
import Image from "next/image";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import styles from "./product-carousel.module.scss";


const ProductCarousel = ({ images, altText }) => {
  return (
    <div className={styles.productCarousel}>
      <Carousel showArrows={false} showStatus={false} showThumbs={false}>
          <div>
              <Image 
                src={`http:${images[0]}`} 
                alt={altText}
                height={600}
                width={500}
                objectFit="cover"
              />
          </div>
          <div>
              <Image 
                src={`http:${images[1]}`}
                alt={altText}
                height={500}
                width={400}
              />
          </div>
          <div>
              <Image 
                src={`http:${images[2]}`}
                alt={altText}
                height={500}
                width={400}
              />
          </div>
      </Carousel>
    </div>
  );
};

export default ProductCarousel;
