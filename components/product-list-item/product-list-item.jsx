import Image from "next/image";
import styles from "./product-list-item.module.scss";

const ProductListItem = ({ image, price, description }) => {
  return (
    <article className={styles.content} data-testid="product-list-item">
      <div>
        <Image 
          src={`http:${image}`}
          alt={description}
          width={400}
          height={500}
          objectFit="cover"
        />
      </div>
      <h2 className={styles.description}>{description}</h2>
      <p className={styles.price}>{price}</p>
    </article>
  );
};

export default ProductListItem;
