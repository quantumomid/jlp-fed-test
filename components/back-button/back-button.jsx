import Image from "next/image";
import { useRouter } from "next/router";
import styles from "./back-button.module.scss";

const BackButton = () => {
    const router = useRouter();
    
    return (
        <button className={styles.arrowButton} onClick={() => router.push("/")}>
            <Image 
                src="/backButton.png"
                alt="Arrow pointing to the left"
                width={15}
                height={30}
            />
        </button>
    )
}

export default BackButton;