import React from "react";
import { render, screen } from "@testing-library/react";
import Home from "@pages/index";
import mockData from "../../../mockData/mockData.json";

describe("First Snapshot Test Case", () => {
    test("Testing the Home page UI", () => {
        const { asFragment } = render(<Home data={mockData} />);
        // screen.debug;
        expect(asFragment()).toMatchSnapshot();
    })
});

describe("HomePage component", () => {
    it("Display text indicating no items found when empty data comes through from server.", () => {
      const {getByText} = render(<Home data={{ }} />);
      const noProductsText = getByText("No items!");  
    //   screen.debug();    
      expect(noProductsText).toBeInTheDocument();
    });

    it("Display 20 Items", () => {
        const {getAllByTestId} = render(<Home data={mockData} />);
        const productListItems = getAllByTestId("product-list-item");  
        // screen.debug();    
        expect(productListItems.length).toBe(20);
      });
})  