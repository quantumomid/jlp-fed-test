# UI Dev Technical Test - Dishwasher App - Omid

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. 

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed NPM using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.

---

## Testing 🧪

Run `npm test` from root directory.

Guide for testing set-up - see [here](https://nextjs.org/docs/testing#setting-up-jest-with-babel) and [here](https://github.com/vercel/next.js/tree/canary/examples/with-jest).

*Made new mockData (copied data object returned from axios) due to differing fields.

## Assumptions 

- Issue (Server Error - error code 500 - authorisation/permissions/cors?) using fetch in getServerSideProps (return to later if time) => using axios for moment being
- Assigned a max width of 1000px 
- Images have cream background in the picture itself
- For the grayish colour, I used #6b6b6b 
- Removed dangerouslysetinnerhtml to avoid risk of cross-site scripting (XSS)
- Landscape wireframe is also assumed to be the desktop screen

## Third party packages

- Axios (see first point in assumptions)
- React Testing Library and Babel-Jest (& identity-obj-proxy) for testing

## Improvements

- Use detail and summary to only show a couple of specification features and only show the full list if user clicks on a "show more/show full list" button
- Product info text could have some sort of ellipsis to cut off after certain word limit with "..." and upon user click the full text appears
- Some sort of image analysis to get backgrounds to be white
- Some processing of specification feature values, i.e. uppercase "YES" to "Yes", etc, in product detail page 
- Better styling with Figma - to be able to see sizes, fonts and weights in better view
- Some processing of data from getServerSideProps before passing along to component
- Write more tests

## If had more time
- Complete breaking product detail page i.e. article component into smaller sub components!
